/* This program is provided under the terms of the MIT License. */

#include <stdio.h>
#include <picam.h>
#include <Windows.h>

int main(int argc, char *argv[])
{
    PicamHandle handle;
    PicamCameraID cameraId;
    PicamError res;
    pibln inited;
    piflt temp; 
    
    inited = 0;
    res = Picam_IsLibraryInitialized(&inited);
    if (res != PicamError_None) {
        fprintf(stderr, "Picam_IsLibraryInitialized failed: %d\n", res);
    }
    if (!inited) {
        res = Picam_InitializeLibrary();
        if (res != PicamError_None) {
            fprintf(stderr, "Picam_InitializeLibrary failed: %d\n", res);
        }
    }
    res = Picam_OpenFirstCamera(&handle);
    if (res != PicamError_None) {
        if (res == PicamError_NoCamerasAvailable) {
            fprintf(stderr, "No Camera Found\n");
        } else {
            fprintf(stderr, "Picam_OpenFirstCamera failed: %d\n", res);
        }
    }
    
    res = Picam_GetCameraID(handle, &cameraId);
    if (res != PicamError_None) {
        fprintf(stderr, "Picam_GetCameraID failed: %d\n", res);
    }
    printf("Camera model: %d\n", cameraId.model);
    printf("Sensor name: %s\n", cameraId.sensor_name);
    printf("Serial number: %s\n", cameraId.serial_number);
    
    for (int i = 1; i <= 1000000; i++) {
        res = Picam_ReadParameterFloatingPointValue(handle, PicamParameter_SensorTemperatureReading, &temp);
        if (res != PicamError_None) {
            fprintf(stderr, "Picam_ReadParameterFloatingPointValue PicamParameter_SensorTemperatureReading failed: %d\n", res);
        }
        printf("cnt: %d temp: %f\n", i, temp);
        if (i == 1) {
            Sleep(10000);
        }
        Sleep(10);
    }
    
    Picam_CloseCamera(handle);
    return 0;
}